//
//  ViewController.swift
//  OxyCustomKeyboard
//
//  Created by Simon Haycock on 01/03/2016.
//  Copyright © 2016 oxygn. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var textField: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: "dismiss")
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func dismiss() {
        self.view.endEditing(true)
    }
}


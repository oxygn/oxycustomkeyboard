//
//  KeyboardViewController.swift
//  Oxygn Keyboard
//
//  Created by Simon Haycock on 01/03/2016.
//  Copyright © 2016 oxygn. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {

    var scrollView: ImageScrollView?
    
    @IBOutlet var nextKeyboardButton: UIButton!

    @IBOutlet weak var imageView: UIImageView!
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
    
        // Add custom view sizing constraints here
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "KeyboardViewController", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        self.view = objects[0] as! UIView
    
        // Perform custom UI setup here
        
        
        
        self.scrollView = ImageScrollView(frame: self.view.frame)
//        self.view.addSubview(self.scrollView!)
        
        /*
        self.nextKeyboardButton = UIButton(type: .Custom)
        let image = UIImage(named: "globe")
        self.nextKeyboardButton.setImage(image, forState: UIControlState.Normal)
        let padding: CGFloat = 10.0
        let edgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        self.nextKeyboardButton.imageEdgeInsets = edgeInsets
        self.nextKeyboardButton.contentMode = UIViewContentMode.ScaleAspectFit
        self.nextKeyboardButton.layer.borderWidth = 1
        self.nextKeyboardButton.layer.cornerRadius = 8.0
//        self.nextKeyboardButton.setTitle(NSLocalizedString("?", comment: ""), forState: .Normal)
        //        self.nextKeyboardButton.sizeToFit()
        self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = false
        
        self.nextKeyboardButton.addTarget(self, action: "advanceToNextInputMode", forControlEvents: .TouchUpInside)
        //        nextKeyboardButton.layer.borderWidth = 2
        self.view.addSubview(self.nextKeyboardButton)
        
        let nextKeyboardButtonLeftSideConstraint = NSLayoutConstraint(item: self.nextKeyboardButton, attribute: .Left, relatedBy: .Equal, toItem: self.view, attribute: .Left, multiplier: 1.0, constant:5.0)
        let nextKeyboardButtonBottomConstraint = NSLayoutConstraint(item: self.nextKeyboardButton, attribute: .Bottom, relatedBy: .Equal, toItem: self.view, attribute: .Bottom, multiplier: 1.0, constant: -5.0)
        self.view.addConstraints([nextKeyboardButtonLeftSideConstraint, nextKeyboardButtonBottomConstraint])
        
        */
        /*
        self.imageView.image = UIImage(named: "mediaLogo")
//        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "longPress:")
//        self.view.addGestureRecognizer(longPressGestureRecognizer)
        */
    }
    /*
    @IBAction func longPressAction(sender: AnyObject) {
        
        let gestureRecognizer = sender as! UILongPressGestureRecognizer
        if gestureRecognizer.state == UIGestureRecognizerState.Began {
//            fatalError("error") // !!!
            /*
            let location = longPressGestureRecognizer.locationInView(longPressGestureRecognizer.view) // silly
            let menuController = UIMenuController.sharedMenuController()
            menuController.setTargetRect(CGRect(x: location.x, y: location.y, width: 0.0, height: 0.0), inView: longPressGestureRecognizer.view!)
            */
//            let tempImage = UIImage(named: "mediaLogo")
            UIPasteboard.generalPasteboard().image = self.imageView.image
//            UIPasteboard.generalPasteboard().image = tempImage
//            let pasteboard = UIPasteboard.generalPasteboard()
//            pasteboard.persistent = true
//            pasteboard.string = "Copied String!!!"
            flashBackground()
            
        }
    }
    */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }

    override func textWillChange(textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }

    override func textDidChange(textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
    
        var textColor: UIColor
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.Dark {
            textColor = UIColor.whiteColor()
        } else {
            textColor = UIColor.blackColor()
        }
        self.nextKeyboardButton.setTitleColor(textColor, forState: .Normal)
    }
    
    /*
    // MARK: - flashing (to be replaced)
    var flashTimer: NSTimer?
    var numberOfFlashes: Int = 0
    let flashTime = 0.1
    let flashLength = 6 // total number of flashes
    var backgroundIsLight: Bool = false
    func flashBackground() {
//        fatalError("error")
        numberOfFlashes = 0
        flashTimer = NSTimer.scheduledTimerWithTimeInterval(flashTime, target: self, selector: "flashTimerFired", userInfo: nil, repeats: true)
        
    }
    func flashTimerFired() {
        numberOfFlashes++
        if numberOfFlashes > flashLength {
            setBackgroundToNormal()
            flashTimer?.invalidate()
            flashTimer = nil
        } else {
            if backgroundIsLight {
                setBackgroundToNormal()
            } else {
                setBackgroundToDark()
            }
        }
    }
    func setBackgroundToNormal() {
        dispatch_async(dispatch_get_main_queue(), {
            self.view.backgroundColor = UIColor.lightGrayColor()
            self.backgroundIsLight = false
        })
        
        
    }
    func setBackgroundToDark() {
        dispatch_async(dispatch_get_main_queue(), {
            self.view.backgroundColor = UIColor.darkGrayColor()
            self.backgroundIsLight = true
        })
        
    }
*/

}


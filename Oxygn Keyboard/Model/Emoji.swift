//
//  Emoji.swift
//  OxyCustomKeyboard
//
//  Created by Simon Haycock on 16/04/2016.
//  Copyright © 2016 oxygn. All rights reserved.
//

import UIKit

class Emoji {
    var image: UIImage
    var name: String
    
    init(name: String, image: UIImage) {
        self.name = name
        self.image = image
    }
    
    func copyToClipboard() {
//        print("emoji with name \(name) copied to clipboard")
        UIPasteboard.generalPasteboard().image = self.image
    }
}
//
//  EmojiView.swift
//  OxyCustomKeyboard
//
//  Created by Simon Haycock on 16/04/2016.
//  Copyright © 2016 oxygn. All rights reserved.
//

import UIKit

class EmojiView: UIImageView {
    
    var emoji: Emoji
//    var imageView: UIImageView
    
    init(frame: CGRect, emoji: Emoji) {
        self.emoji = emoji
//        self.imageView = UIImageView(image: self.emoji.image)
//        self.imageView.frame = frame
        super.init(frame: frame)
        self.image = emoji.image
//        self.addSubview(self.imageView)
        self.userInteractionEnabled = true
        
//        self.autoresizesSubviews = true
        self.contentMode = UIViewContentMode.ScaleAspectFit
        self.layer.borderWidth = 1
//        self.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        // not so sure about this. if you are holding down finger on emoji, it would be better to override touches began / ended, so as to show preview, and copy when finger is released
        let tapGesture = UITapGestureRecognizer(target: self, action: "tap:")
        self.addGestureRecognizer(tapGesture)
        
//        finger down:
//        larger version of emoji above, but same x position, with “copy to clipboard” text.
//        finger up:
//        same, but text is “now paste in your app”, and fades
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tap(tapGestureRecognizer: UITapGestureRecognizer) {
        print("tap in emoji with image name \(self.emoji.name). copying to clipboard...")
        self.emoji.copyToClipboard()
    }
    
    override func layoutSubviews() {
        print("layoutSubviews called for emoji with image name \(self.emoji.name)")
        super.layoutSubviews()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("touches began")
    }
}

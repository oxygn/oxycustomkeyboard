//
//  ImageScrollView.swift
//  OxyCustomKeyboard
//
//  Created by Simon Haycock on 15/04/2016.
//  Copyright © 2016 oxygn. All rights reserved.
//

import UIKit

class ImageScrollView : UIView {
    
    let scrollView: UIScrollView
    let contentView: UIView
    
    // set during init TODO: (would be nice to set them here, but can't be accessed during init?)
    var numberOfRows: CGFloat
    var numberOfImages: Int // TODO: make this dependant on an image repository
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("initWithCoder not implemented")
    }
    
    override init(frame: CGRect) {
        self.scrollView = UIScrollView(frame: frame)
        self.contentView = UIView()
        numberOfRows = 2
        numberOfImages = 10
        super.init(frame: frame)
        
        self.autoresizesSubviews = true
        self.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.scrollView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        let contentSize = self.contentSizeForScrollViewWithRows(numberOfRows, numberOfImages: numberOfImages)
        let contentFrame = CGRectMake(0, 0, contentSize.width, contentSize.height)
        self.contentView.frame = contentFrame
//        self.contentView.layer.borderWidth = 3
        self.contentView.clipsToBounds = true
        self.scrollView.userInteractionEnabled = true
        
        scrollView.addSubview(contentView) // TODO: uncomment
        
        
        // variables for loop
        let widthAndHeightOfOneImage = frame.size.height / numberOfRows // not needed...
        var xPos: CGFloat = 0
        var yPos: CGFloat = 0
        var viewArray: [UIView] = []
        var constraintArray: [NSLayoutConstraint] = []
        
        for index in 1...numberOfImages {

            // TODO: frame is not important, as this is determined by constraints
            let emojiFrame = CGRectMake(0, 0, widthAndHeightOfOneImage, widthAndHeightOfOneImage)
            
            // set image name
            let imageName = String(index)
            
            // instantiate image
            guard let image = UIImage(named: imageName) else {
                fatalError("no emoji named \(imageName)")
            }
            
            // instantiate emoji view
            // TODO: could give emoji a name, rather than UIImage. will be easier to dynamically load the image data
            let emoji = Emoji(name: imageName, image: image)
            let emojiView = EmojiView(frame: emojiFrame, emoji: emoji)
            emojiView.translatesAutoresizingMaskIntoConstraints = false
//            emojiView.autoresizesSubviews = true
            
            contentView.addSubview(emojiView) // TODO: uncomment
            
            // position in array  = (xPos * 3) + yPos
            
            // add constraints
            
            // size: always a multiple of contentView's height, regardless of position
            let widthMultiplier = 1.0 / numberOfRows
            let widthConstraint = NSLayoutConstraint(item: emojiView, attribute: .Width, relatedBy: .Equal, toItem: contentView, attribute: .Height, multiplier: widthMultiplier, constant: 0)
            constraintArray.append(widthConstraint)
            let heightConstraint = NSLayoutConstraint(item: emojiView, attribute: .Height, relatedBy: .Equal, toItem: contentView, attribute: .Height, multiplier: widthMultiplier, constant: 0)
            constraintArray.append(heightConstraint)
            
            if (xPos == 0) {
                // left-most views have no view to the left to have a constraint to, so are treated differently
                
                // x position: leading margin stuck to contentView leading
                let topLeftLeadingViewConstraint = NSLayoutConstraint(item: emojiView, attribute: .Leading, relatedBy: .Equal, toItem: contentView, attribute: .Top, multiplier: 1.0, constant: 0)
                constraintArray.append(topLeftLeadingViewConstraint)
                
                if (yPos == 0) {
                    // y position: if top left, align y to top
                    let topLeftTopViewConstraint = NSLayoutConstraint(item: emojiView, attribute: .Top, relatedBy: .Equal, toItem: contentView, attribute: .Top, multiplier: 1.0, constant: 0)
                    constraintArray.append(topLeftTopViewConstraint)
                } else {
                    // stick view to view above
                    let viewAbove = viewArray[Int(yPos - 1)]
                    let yPosConstraint = NSLayoutConstraint(item: emojiView, attribute: .Top, relatedBy: .Equal, toItem:
                     viewAbove, attribute: .Bottom, multiplier: 1.0, constant: 0)
                    constraintArray.append(yPosConstraint)
                }
                
            } else {
                // if xPos is over 0, there is always a view to the left which can be used for x and y positioning
                
                // x position: stick to view on the immediate left
                let arrayPosition = Int( ((xPos - 1) * numberOfRows) + yPos )
                let viewToTheLeft = viewArray[arrayPosition]
                let xPosConstraint = NSLayoutConstraint(item: emojiView, attribute: .Leading, relatedBy: .Equal, toItem: viewToTheLeft, attribute: .Trailing, multiplier: 1.0, constant: 0)
                constraintArray.append(xPosConstraint)
                
                // y position: same as view to the left
                let yPosConstraint = NSLayoutConstraint(item: emojiView, attribute: .Top, relatedBy: .Equal, toItem: viewToTheLeft, attribute: .Top, multiplier: 1.0, constant: 0)
                constraintArray.append(yPosConstraint)
                
            }


            viewArray.append(emojiView)
            
            // move on to next position
            yPos++
            if (yPos == numberOfRows) {
                // new line
                yPos = 0
                xPos++
            }
            
        }
        
        NSLayoutConstraint.activateConstraints(constraintArray)
        
        
        self.addSubview(scrollView) // TODO: uncomment

        
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        // update content frame when view is asked to layout subviews
        
        let contentSize = self.contentSizeForScrollViewWithRows(numberOfRows, numberOfImages: numberOfImages)
        contentView.frame = CGRectMake(0, 0, contentSize.width, contentSize.height)
        self.scrollView.contentSize = contentSize

        
    }
    
    func contentSizeForScrollViewWithRows(numberOfRows: CGFloat, numberOfImages: Int) -> CGSize {
        let widthAndHeightOfOneImage = frame.size.height / numberOfRows
        let numberOfColums = ceil(CGFloat(numberOfImages) / numberOfRows)
        let contentSize = CGSizeMake(numberOfColums * widthAndHeightOfOneImage, numberOfRows * widthAndHeightOfOneImage)
        return contentSize
    }
    
}
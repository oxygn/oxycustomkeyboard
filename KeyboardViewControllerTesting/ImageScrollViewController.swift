//
//  ViewController.swift
//  KeyboardViewControllerTesting
//
//  Created by Simon Haycock on 15/04/2016.
//  Copyright © 2016 oxygn. All rights reserved.
//

import UIKit

class ImageScrollViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        self.view.backgroundColor = UIColor.greenColor()
        let imageScrollView = ImageScrollView(frame: self.view.frame)
        self.view.addSubview(imageScrollView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

